;;; crystal+.el -*- lexical-binding: t; -*-

(use-package crystal-mode
  :defer t)

(provide 'crystal+)
;;; crystal+.el ends here
