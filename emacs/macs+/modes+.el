;;; modes+.el -*- lexical-binding: t; -*-

(defun maps+ (modes)
  (cl-loop for mode in modes
           collect (derived-mode-map-name mode)))

(defun syntax+ (&optional mode &rest chars)
  "More convenient form of `modify-syntax-entry'."
  (when chars
    (modify-syntax-entry
     (pop chars) (pop chars)
     (symbol-value (derived-mode-syntax-table-name mode)))
    (apply #'syntax+ mode chars)))

(use-package guess-style
  :straight
  (guess-style
   :type git
   :host github
   :repo "nschum/guess-style")
  :config (global-guess-style-info-mode +1)
  :defer t)

(use-package handle
  :straight
  (handle
   :type git
   :host gitlab
   :repo "jjzmajic/handle")
  :commands handle
  :init
  (setq handle-keywords
        '(
          ;; +++
          :compilers
          :docs
          :errors
          :evaluators
          :evaluators-line
          :evaluators-paragraph
          :evaluators-selection
          :formatters
          :gotos
          :repls
          ;; +++
          ))
  :general
  (kbd+
    :infix "c"
    "k" '(handle-docs :wk "doc")
    "g" '(handle-gotos :wk "goto")
    "r" '(handle-repls :wk "repl")
    "f" '(handle-formatters :wk "format")
    "e" '(handle-evaluators :wk "eval")
    "l" '(handle-evaluators-line :wk "eval line")
    "p" '(handle-evaluators-paragraph :wk "eval paragraph")
    "s" '(handle-evaluators-selection :wk "eval selection")
    "c" '(handle-compilers :wk "compile")))

(use-package quickrun
  :general
  (kbd+ "hq"
    '(helm-quickrun
      :wk "helm quickrun")))

(use-package pastebin
  :general (kbd+ "fp" #'pastebin))
(use-package string-inflection
  :defer t)


(hook+ 'prog-mode-hook
       '(column-number-mode
         hs-minor-mode))
(handle '(prog-mode text-mode)
        :docs #'dumb-jump
        :compilers #'imake
        :formatters #'indent-region
        :evaluators #'quickrun)

(provide 'modes+)
;;; modes+.el ends here
