;;; racket+.el -*- lexical-binding: t; -*-

(defvar racket+modes '(racket-mode racket-repl-mode))
(defvar racket+maps '(racket-mode-map racket-repl-mode-map))

;; TODO: Implement this [[https://www.racket-mode.com/#Install]]
(use-package racket-mode
  :general
  (kbd+local :keymaps racket+maps
    kbd+localleader #'racket-send-last-sexp)
  :gfhook #'racket-eldoc-function
  :config
  (after+ 'handle
    (handle racket+modes
	    :repls #'racket-repl
	    :evaluators #'racket-run))
  (after+ 'compdef
    (compdef :modes racket+modes
	     :company '(company-capf company-dabbrev-code)
	     :capf #'racket-complete-at-point)))

(provide 'racket+)
;;; racket+.el ends here
