;;; python+.el -*- lexical-binding: t; -*-

(defvar python+modes
  '(python-mode
    jupyter-repl-mode))

(defvar python+maps
  (maps+ python+modes))

(defun python+send-paragraph ()
  (interactive)
  (when-let* ((bounds (bounds-of-thing-at-point 'sentence)))
    (cl-destructuring-bind (beg . end) bounds
      (jupyter-eval-region beg end)
      (and (forward-thing 'sentence +2)
           (forward-thing 'sentence -1)))))

(use-package pipenv
  :ghook 'python-mode-hook
  :init (setq pipenv-projectile-after-switch-function
              #'pipenv-projectile-after-switch-default))

(use-package jupyter
  :init (setq jupyter-repl-echo-eval-p t)
  :general (kbd+local :keymaps python+maps
             kbd+localleader
             #'python+send-paragraph))

(after+ 'handle
  (handle python+modes
          :repls #'jupyter-run-repl
          :evaluators #'jupyter-eval-buffer
          :evaluators-line #'jupyter-eval-line-or-region
          :evaluators-selection #'jupyter-eval-line-or-region
          :docs '(jupyter-inspect-at-point
                  lsp-describe-thing-at-point)))

(after+ 'lsp+
  (hook+ 'python-mode-hook
         #'lsp-deferred))

(after+ 'compdef
  (compdef
   :modes #'python-mode
   :company '((company-lsp company-capf))
   :capf '(jupyter-completion-at-point))
  (compdef
   :modes #'jupyter-repl-mode
   :capf '(jupyter-completion-at-point)
   :company '(company-capf)))

(provide 'python+)
;;; python+.el ends here
