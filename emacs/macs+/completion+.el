;;; completion+.el -*- lexical-binding: t; -*-

(fset 'completion+at-point
      (indirect-function #'completion-at-point))

(defun completion+company nil
  (interactive)
  (unless company-mode (company-mode +1))
  (if (minibufferp) (completion+at-point) (helm-company)))

(use-package compdef
  :defer t
  :straight (compdef
             :type git
             :host gitlab
             :repo "jjzmajic/compdef"
             :branch "develop"))

(use-package company
  :init (setq company-idle-delay nil)
  :config (global-company-mode))

(use-package helm-company
  :after helm+
  :config
  (when helm+fuzzy (setq helm-company-fuzzy-match t))
  ;; emulate (setq tab-always-indent 'complete)
  (setq-default tab-always-indent 'complete)
  (advice+ #'completion-at-point
           :override #'completion+company))

(provide 'completion+)
;;; company+.el -*- lexical-binding: t; -*-
