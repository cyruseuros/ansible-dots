;;; cpp+.el -*- lexical-binding: t; -*-

(use-package irony :ghook 'c-mode-common-hook)
(use-package irony-eldoc :ghook 'c-mode-common-hook)
(use-package company-irony :defer t)
(use-package flycheck-irony :defer t)

(after+ 'cc-mode
  (hook+ 'c-mode-common-hook
         #'guess-style-guess-all)
  (general-def :modes '(c-mode c++-mode)
    [tab] #'indent-for-tab-command))

(after+ 'compdef
  (compdef
   :modes '(c-mode c++-mode)
   :company
   '((company-irony
      company-capf
      company-dabbrev-code))))

(provide 'cpp+)
;;; cpp+.el ends here
