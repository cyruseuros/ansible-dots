;;; snippets+.el -*- lexical-binding: t; -*-

(use-package yasnippet-snippets
  :ghook ('yas-minor-mode-hook
           #'yasnippet-snippets-initialize
           nil nil t))

(use-package yasnippet
  :ghook ('prog-mode-hook #'yas-minor-mode)
  :config (yas-reload-all)
  :general (kbd+ "is"
             '(yas-insert-snippet
               :wk "insert snippet")))

(provide 'snippets+)
;;; snippets+.el ends here
