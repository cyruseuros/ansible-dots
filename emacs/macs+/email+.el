;;; email+.el -*- lexical-binding: t; -*-

(setq user-mail-address "uros.perisic@u.yale-nus.edu.sg"
      user-full-name "Uros Perisic")

(use-package mu4e
  :init (setq mu4e-maildir (dirs+sync "maildir"))
  :general (kbd+ "m" #'mu4e))

(provide 'email+)
;;; email+.el ends here
