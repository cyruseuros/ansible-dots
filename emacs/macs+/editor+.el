;;; editor+.el -*- lexical-binding: t; -*-

(setq-default
 indent-tabs-mode nil
 tab-width 4)

(use-package iedit
  :general
  (kbd+ "e" '(iedit-mode :wk "iedit"))
  :config
  (setq iedit-mode-keymap
        (make-sparse-keymap))
  (general-def
    :keymaps 'iedit-mode-keymap
    :states '(normal visual)
    "s" #'iedit-show/hide-unmatched-lines
    "t" #'iedit-toggle-selection
    "n" #'iedit-next-occurrence
    "p" #'iedit-prev-occurrence
    "q" #'iedit-mode)
  (after+ 'hercules
    (hercules-def
     :toggle-funs #'iedit-mode
     :keymap 'iedit-mode-keymap)))

(hook+
 'before-save-hook
 #'delete-trailing-whitespace)
(electric-pair-mode +1)
(kbd+ "xs" '(sort-lines :wk "sort lines"))

(provide 'editor+)
;;; editor+.el ends here
