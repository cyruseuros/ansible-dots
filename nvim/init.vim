" modules
let module_path=stdpath('config') . '/modules'
execute 'set runtimepath+='.module_path

runtime plg.vim
runtime kbd.vim
runtime vim.vim
runtime rc.vim
runtime dnt.vim
" runtime cc.vim
