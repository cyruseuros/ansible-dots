---
- hosts: localhost
  become: yes
  tasks:
    - name: update system
      xbps: upgrade=yes
    - name: install pkgs
      block:
        - name: binary pkgs
          xbps:
            state: present
            name:
              # pdf
              - poppler
              - poppler-devel
              - poppler-glib
              - poppler-glib-devel
              - Adapta
              - ImageMagick
              - python3-subliminal
              # printing
              - cups
              - hplip
              - libcups
              - cups-filters
              - system-config-printer
              # networking
              - connman
              - bluez
              - openvpn
              - connman-gtk
              - NetworkManager
              - NetworkManager-openvpn
              - NetworkManager-pptp
              # emacs development
              - gtk+3-devel
              - libXpm-devel
              - gnutls-devel
              - giflib-devel
              - libgit2
              - shellcheck
              - alsa-plugins-pulseaudio
              - aria2
              - ark
              - aspell
              - aspell-en
              - biber
              - blueman
              - breeze-cursors
              - bspwm
              - cmus
              - opendoas
              - docker
              - direnv
              - docker-compose
              - emacs-gtk3
              - firefox
              - fish-shell
              - font-adobe-source-code-pro
              - freerdp
              - git
              - gnupg
              - rustup
              # c-lang
              - llvm
              - gdb
              - libgomp
              - libgomp-devel
              - clang
              - clang-analyzer
              - clanf-tools-extra
              - man-pages-devel
              # r-lang
              - R
              - libcurl-devel
              - libressl-devel
              - libxml2-devel
              - zlib-devel
              # desktop
              # - lightdm-gtk-greeter-settings
              - libreoffice
              - libvirt
              - libvncserver
              - lxqt
              - mlocate
              - neovim
              - nerd-fonts
              - nodejs
              - pamixer
              - pandoc
              - papirus-icon-theme
              - perl-rename
              - polybar
              - progress
              - pulseaudio
              # python
              - python3-language-server
              - python3-neovim
              - python3-jupyter
              - python3-pandas
              - python3-matplotlib
              - python3-pipenv
              # desktop
              - pywal
              - qt5-styleplugins
              - rofi
              - zathura
              - zathura-pdf-poppler
              - scrot
              - slock
              - sxhkd
              - udevil
              - syncthing
              - ttf-ubuntu-font-family
              - xbacklight
              - xclip
              - xev
              - xinput
              - xrandr
              - xrdb
              - xsetroot
              # virtualization
              - qemu
              - rdesktop
              - socklog-void
              - stow
              - tectonic
              - the_silver_searcher
              - virt-manager
              - xtools
              - zip
              - zsh
        - name: clone void source packages
          tags: void_packages
          register: void_packages
          git:
            repo: https://github.com/void-linux/void-packages.git
            dest: ~/src/void-packages
            update: no
        - name: install void source packages
          tags: void_packages
          when: void_packages.changed
          command: ~/src/void-packages/xbps-src binary-bootstrap
    - name: latex
      block:
        - name: install texlive
          tags: texlive
          register: texlive
          xbps:
            name: texlive-bin
            state: present
        - name: update tlmgr
          tags: texlive
          become_method: doas
          when: texlive.changed
          command: tlmgr update --self
        - name: install texlive collections
          tags: texlive
          become_method: doas
          when: texlive.changed
          command: >
            tlmgr install
            nacollection-basic
            collection-bibtexextra
            collection-binextra
            collection-context
            collection-fontsextra
            collection-fontsrecommended
            collection-fontutils
            collection-formatsextra
            collection-games
            collection-humanities
            collection-langarabic
            collection-langchinese
            collection-langcjk
            collection-langcyrillic
            collection-langczechslovak
            collection-langenglish
            collection-langeuropean
            collection-langfrench
            collection-langgerman
            collection-langgreek
            collection-langitalian
            collection-langjapanese
            collection-langkorean
            collection-langother
            collection-langpolish
            collection-langportuguese
            collection-langspanish
            collection-latex
            collection-latexextra
            collection-latexrecommended
            collection-luatex
            collection-mathscience
            collection-metapost
            collection-music
            collection-pictures
            collection-plaingeneric
            collection-pstricks
            collection-publishers
            collection-texworks
            collection-xetex
    - name: enable services
      runit:
        name: "{{ item }}"
        enabled: yes
        state: started
      with_items:
        - NetworkManager
        # - connmand
        - acpid
        - agetty-tty1
        - agetty-tty2
        - agetty-tty3
        - agetty-tty4
        - agetty-tty5
        - agetty-tty6
        - bluetoothd
        - dbus
        - cupsd
        - docker
        - libvirtd
        - nanoklogd
        - polkitd
        - rtkit
        - socklog-unix
        - sshd
        - udevd
        - uuidd
        - virtlockd
        - virtlogd
    - name: check whether wheel group can doas
      lineinfile:
        path: /etc/doas.conf
        state: present
        line: 'permit persist :wheel'
        validate: /bin/doas -C %s
        create: yes
    # - name: install rust
    #   rustup: default stable
    #   rustup: component add rls rust-analysis rust-src
